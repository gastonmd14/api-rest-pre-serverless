require('dotenv').config()
const mongoose = require('mongoose');

const options = {
    autoIndex: false,
    maxPoolSize: 10,
    serverSelectionTimeoutMS: 5000,
    socketTimeoutMS: 45000,
    family: 4,
    useNewUrlParser: true, 
    useUnifiedTopology: true 
};

const URI = `mongodb://${process.env.DB_HOST}/${process.env.DB_NAME_DEV}`;

mongoose.connect(URI, options);

const dbConnection = mongoose.connection;

dbConnection.on('connected', function () {
    console.log('Mongoose connected to ' + URI);
});

dbConnection.on('error',function (err) {
    console.log('Mongoose connection error: ' + err);
});
  
dbConnection.on('disconnected', function () {
    console.log('Mongoose disconnected');
});
  
process.on('SIGINT', function() {
    dbConnection.close(function () {
        console.log('Mongoose disconnected through app termination');
    process.exit(0);
    });
});

module.exports = {
    dbConnection
}
