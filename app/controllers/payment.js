const paymentRepository = require('../services/databases/payment')

const savePayment = async (req, res) => {
    try {
        const { userId, invoiceId } = req.body;
        const result = await paymentRepository.savePaymentTransaction(userId, invoiceId);
        return res.status(200).json({ payment: result });
    } catch (error) {
        return res.status(500).json({ errors: error });;
    }
};

module.exports = {
    savePayment
};
