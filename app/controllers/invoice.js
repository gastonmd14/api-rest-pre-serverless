const invoiceRepository = require('../services/databases/invoice')

const saveInvoice = async (req, res) => {
    try {
        const { product, price } = req.body;
        const businessId = req.params.id;
        const result = await invoiceRepository.save(businessId, product, price);
        return res.status(200).json({ invoice: result });
    } catch (error) {
        return res.status(500).json({ errors: error });;
    }
};

module.exports = {
    saveInvoice
};
