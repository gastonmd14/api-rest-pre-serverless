const businessRepository = require('../services/databases/business')

const listAll = async (req, res) => {
    try {
        const result = await businessRepository.getAll({});
        return res.status(200).json({ business: result });
    } catch (error) {
        return res.status(500).json({ errors: error });;
    }
}; 

const listById = async (req, res) => {
    try {
        const id = req.params.id;
        const result = await businessRepository.getById(id);
        return res.status(200).json({business: result });
    } catch (error) {
        return res.status(500).json({ errors: error });;
    }
};

const listByEmail = async (req, res) => {
    try {
        const email = req.body.email;
        const result = await businessRepository.getByEmail(email);
        return res.status(200).json({ business: result });
    } catch (error) {
        return res.status(500).json({ errors: error });;
    }
};

const saveOne = async (req, res) => {
    try {
        const business = req.body;
        console.log(business);
        const result = await businessRepository.save(business);
        return res.status(200).json({ business: result });
    } catch (error) {
        return res.status(500).json({ errors: error });;
    }
};

const updateOne = async (req, res) => {
    try {
        const id = req.params.id;
        const business = req.body;
        const result = await businessRepository.update(id, business);
        return res.status(200).json({ business: result });
    } catch (error) {
        return res.status(500).json({ errors: error });;
    }
};

const destroyOne = async (req, res) => {
    try {
        const id = req.params.id;
        const result = await businessRepository.destroy(id);
        return res.status(200).json({ business: result });
    } catch (error) {
        return res.status(500).json({ errors: error });;
    }
};

module.exports = {
    listAll,
    listById,
    listByEmail,
    saveOne,
    updateOne,
    destroyOne
};
