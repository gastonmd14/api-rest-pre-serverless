const userRepository = require('../services/databases/user');
const businessRepository = require('../services/databases/business');

const createToken = require('../services/internals/createToken');

const signIn = async (req, res) => {
    try {
        if (req.body.isBusiness === 'true') {
            const businessEmail = req.body.email;
            const result = await businessRepository.getByEmail(businessEmail);
            const newToken = createToken(result);
            return res.status(200).json({ business: result, token: newToken });
        } else {
            const userEmail = req.body.email;
            const result = await userRepository.getByEmail(userEmail);
            const newToken = createToken(result);
            return res.status(200).json({ user: result, token: newToken  });
        }
    } catch (error) {
        return res.status(500).json({ errors: error });;
    }
};

const signUp = async (req, res) => {
    try {
        if(req.body.isBusiness === 'true') {
            const business = req.body;
            const result = await businessRepository.save(business);
            return res.status(200).json({ business: result._id });
        } else {
            const user = req.body;
            const result = await userRepository.save(user);
            return res.status(200).json({ user: result._id });
        }
    } catch (error) {
        return res.status(500).json({ errors: error });;
    }
};

module.exports = {
    signIn,
    signUp
};
