const userRepository = require('../services/databases/user')

const listAll = async (req, res) => {
    try {
        const result = await userRepository.getAll({});
        return res.status(200).json({ users: result });
    } catch (error) {
        return res.status(500).json({ errors: error });;
    }
}; 

const listById = async (req, res) => {
    try {
        const id = req.params.id;
        const result = await userRepository.getById(id);
        return res.status(200).json({ user: result });
    } catch (error) {
        return res.status(500).json({ errors: error });;
    }
};

const listByEmail = async (req, res) => {
    try {
        const email = req.body.email;
        const result = await userRepository.getByEmail(email);
        return res.status(200).json({ user: result });
    } catch (error) {
        return res.status(500).json({ errors: error });;
    }
};

const saveOne = async (req, res) => {
    try {
        const user = req.body;
        const result = await userRepository.save(user);
        return res.status(200).json({ user: result });
    } catch (error) {
        return res.status(500).json({ errors: error });;
    }
};

const updateOne = async (req, res) => {
    try {
        const id = req.params.id;
        const user = req.body;
        const result = await userRepository.update(id, user);
        return res.status(200).json({ user: result });
    } catch (error) {
        return res.status(500).json({ errors: error });;
    }
};

const destroyOne = async (req, res) => {
    try {
        const id = req.params.id;
        const result = await userRepository.destroy(id);
        return res.status(200).json({ user: result });
    } catch (error) {
        return res.status(500).json({ errors: error });;
    }
};

module.exports = {
    listAll,
    listById,
    listByEmail,
    saveOne,
    updateOne,
    destroyOne
};
