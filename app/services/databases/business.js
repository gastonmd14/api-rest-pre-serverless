const BusinessModel = require('../../models/business');

const getAll = async () => await BusinessModel.find({}).select('-password');

const getById = async (id) => await BusinessModel.findById(id).select('-password');

const getByEmail = async (email) => await BusinessModel.findOne({email: email}).lean().select('-password');

const save = async (business) => await BusinessModel.create(business);

const update = async (id, business) =>  await BusinessModel.findOneAndUpdate({ _id: id }, business, { new: true }).select('-password');
  
const destroy = async (id) => await BusinessModel.findOneAndDelete(id).select('-password');

module.exports = {
    getAll,
    getById,
    getByEmail,
    save,
    update,
    destroy
};
