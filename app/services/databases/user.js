const UserModel = require('../../models/user');

const getAll = async () => await UserModel.find({}).select('-password');

const getById = async (id) => await UserModel.findById(id).select('-password');

const getByEmail = async (email) => await UserModel.findOne({email: email}).lean().select('-password');

const save = async (user) => await UserModel.create(user);

const update = async (id, user) =>  await UserModel.findOneAndUpdate({ _id: id }, user, { new: true }).select('-password');
  
const destroy = async (id) => await UserModel.findOneAndDelete(id).select('-password');

module.exports = {
    getAll,
    getById,
    getByEmail,
    save,
    update,
    destroy
};
