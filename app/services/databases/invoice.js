const InvoiceModel = require('../../models/invoice');

const save = async (businessId, product, price) => await InvoiceModel.create({ businessId: businessId, product: product, price: price });

module.exports = {
    save
};
