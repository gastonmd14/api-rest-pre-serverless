const InvoiceModel = require('../../models/invoice');
const PaymentModel = require('../../models/payment');
const UserModel = require('../../models/user');

const mongoose = require('mongoose');

async function incrementRecord(findInvoice) {

    if(findInvoice.record < 2) {

        if(findInvoice.status === 'pending') {

            let totalRecord = findInvoice.record + 1;
    
            const updateInvoice = await InvoiceModel.findOneAndUpdate(
                { _id: findInvoice._id },
                { record: totalRecord },
                { new: true }
            );
    
            return updateInvoice;
        }
    
        return false;
    }

    let totalRecord = findInvoice.record + 1;

    const updateInvoice = await InvoiceModel.findOneAndUpdate(
        { _id: findInvoice._id },
        {
            record: totalRecord,
            status: 'blocked'
        },
        { new: true }
    );

    return updateInvoice;
}

async function paymentError(hasLimit, cardAvailable, notDebt ) {
    return { 
        error: 'payment transaction fail',
        userError: {
            cardError: {
                'limit': hasLimit,
                'status': cardAvailable,
                'debt': notDebt
            }
        } 
    };
}

async function invoiceError(findInvoice) {
    return { 
        error: 'payment transaction fail',
                invoiceError: {
                    status: findInvoice.status,
                    record: findInvoice.record
                }  
    };
}

async function makePayment(findUser, findInvoice, totalLimit) {

    let totalRecord = findInvoice.record + 1;

    const paymentTransaction = await PaymentModel.create(
        {
            userId: findUser._id,
            invoiceId: findInvoice._id
        }
    );

    const updateUser = await UserModel.findOneAndUpdate(
        { _id: findUser._id }, 
        { $set: { 'card.limit': totalLimit } }, 
        { new: true }
    );

    const updateInvoice = await InvoiceModel.findOneAndUpdate(
        { _id: findInvoice._id },
        { userId: findUser._id, status: 'completed', record: totalRecord },
        { new: true }
    );

    return paymentTransaction;
}

const savePaymentTransaction = async (userId, invoiceId) => {

    try {

        const session = await mongoose.startSession();

        session.startTransaction();

        const findInvoice = await InvoiceModel.findById(invoiceId);

        if (findInvoice) {

            const isAvailable = findInvoice ? findInvoice.status === 'pending' : false;
            const notBlocked = isAvailable ? findInvoice.record < 3 : false;

            if(notBlocked) {

                const findUser = await UserModel.findById(userId);

                if(findUser) {

                    const totalLimit = findUser.card.limit - findInvoice.price;

                    const hasLimit = totalLimit >= 0 ? true : false;
                    const cardAvailable = hasLimit ? findUser.card.status === 'active' : false;
                    const notDebt = cardAvailable ? findUser.card.debt <= 0 : false;

                    if (notDebt) { 

                        await session.commitTransaction();

                        makePayment(findUser, findInvoice, totalLimit);

                        session.endSession();

                    } else {

                        incrementRecord(findInvoice);
                
                        session.endSession();

                        return paymentError(hasLimit, cardAvailable, notDebt);

                    }

                } else {

                    incrementRecord(findInvoice);
                
                    session.endSession();

                    return { error: 'user not found'};

                }  

            } else {

                incrementRecord(findInvoice);

                session.endSession();

                return invoiceError(findInvoice);

            }

        } else {
            
            session.endSession();

            return { error: 'invoice not found'};

        }

    } catch (error) {

        await session.abortTransaction();

        session.endSession();

        return { databaseError:  error };

    }

}

module.exports = {
    savePaymentTransaction
}
