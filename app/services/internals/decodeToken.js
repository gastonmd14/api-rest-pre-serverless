const jwt = require('jsonwebtoken');

const decodeUserToken = (token) => {
    const tokenDecoded = jwt.verify(token, process.env.TOKEN_KEY, function(err, decoded) {
        if(decoded) {
            return decoded;
        }
        return err;
    }); 
    return tokenDecoded;   
};

module.exports = decodeUserToken;
