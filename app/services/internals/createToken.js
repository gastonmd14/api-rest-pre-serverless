const jwt = require('jsonwebtoken');

const createUserToken = (payload) => {
    const token = jwt.sign(
        payload, 
        process.env.TOKEN_KEY,
        {
            algorithm: 'HS256',
            expiresIn: '1h',
            notBefore: '60'
        })
    return token;
};

module.exports = createUserToken;
