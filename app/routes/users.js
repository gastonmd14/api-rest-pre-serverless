const express = require('express');
const router = express.Router();

const { listAll, listById, updateOne, destroyOne } = require('../controllers/user');
const { validateUser, authenticateUser } = require('../middlewares/user');

router.get('/', validateUser, listAll);
router.get('/:id', validateUser, listById);
router.put('/:id', validateUser, authenticateUser, updateOne);
router.delete('/:id', validateUser, authenticateUser, destroyOne);

module.exports = router;
