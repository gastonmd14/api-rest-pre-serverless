const express = require('express');
const router = express.Router();

const { validateSignIn, validateSignUp } = require('../middlewares/auth');
const { signIn, signUp  } = require('../controllers/auth');

router.post('/login', validateSignIn, signIn);
router.post('/register', validateSignUp, signUp);

module.exports = router;
