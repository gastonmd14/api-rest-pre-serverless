const express = require('express');
const router = express.Router();

const { savePayment } = require('../controllers/payment');
const { validateUser } = require('../middlewares/user');

router.post('/', validateUser, savePayment);

module.exports = router;
