const express = require('express');
const router = express.Router();

const { listAll, listById, updateOne, destroyOne } = require('../controllers/business');
const { saveInvoice } = require('../controllers/invoice');
const { validateBusiness, authenticateBusiness } = require('../middlewares/business');

router.get('/', validateBusiness, listAll);
router.get('/:id', validateBusiness, listById);
router.put('/:id', validateBusiness, authenticateBusiness, updateOne);
router.delete('/:id', validateBusiness, authenticateBusiness, destroyOne);

router.post('/:id/invoices', validateBusiness, authenticateBusiness, saveInvoice)

module.exports = router;
