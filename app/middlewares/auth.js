const { checkSchema, validationResult } = require('express-validator');
const { fail } = require('../services/internals/constants');
const UserModel = require('../models/user');
const BusinessModel = require('../models/business');

async function validateSignUp(req, res, next) {
    await checkSchema({
        email: {
            exists: true,
            notEmpty: {
                negated: false,
                errorMessage: fail.EMPTY
            },
            customSanitizer: {
                options: async (value, {req}) => {
                    if(req.body.isBusiness === 'true') {
                        await BusinessModel.findOne({ email: value}).then(user => {
                            if(user) {
                                value = fail.EMAIL_DUPLICATE;
                            }
                        })
                    } else {
                        await UserModel.findOne({ email: value}).then(user => {
                            if(user) {
                                value = fail.EMAIL_DUPLICATE;
                            }
                        })
                    }
                return value;
                }
            },
            isEmail: true,
        },
        password: {
            exists: true,
            isLength: {
                errorMessage: fail.PASSWORD_LONG,
                options: {
                    min: 8
                }
            },
            notEmpty: {
                negated: false,
                errorMessage: fail.EMPTY
            }
        },
        isBusiness: {
            exists: true,
            notEmpty: {
                negated: false,
                errorMessage: fail.EMPTY
            }
        }
    }).run(req);
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    return next();
};

async function validateSignIn(req, res, next) {
    await checkSchema({
        email: {
            exists: true,
            notEmpty: {
                negated: false,
                errorMessage: fail.EMPTY
            },
            customSanitizer: {
                options: async (value, {req}) => {
                    if(req.body.isBusiness === 'true') {
                        await BusinessModel.findOne({ email: value}).then(user => {
                            if(!user) {
                                value = fail.WRONG_PARAMS;
                            }
                        })
                    } else {
                        await UserModel.findOne({ email: value}).then(user => {
                            if(!user) {
                                value = fail.WRONG_PARAMS;
                            }
                        })
                    }
                return value;
                }
            },
            isEmail: true,
        },
        password: {
            exists: true,
            isLength: {
                errorMessage: fail.PASSWORD_LONG,
                options: {
                    min: 8
                }
            },
            notEmpty: {
                negated: false,
                errorMessage: fail.EMPTY
            }
        },
        isBusiness: {
            exists: true,
            notEmpty: {
                negated: false,
                errorMessage: fail.EMPTY
            }
        }
    }).run(req);
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    return next();
};

module.exports = {
    validateSignUp,
    validateSignIn
};
