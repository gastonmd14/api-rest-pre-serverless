const jwt = require('jsonwebtoken');
const decodeToken = require('../services/internals/decodeToken');
const { fail } = require('../services/internals/constants');

const validateBusiness = (req, res, next) => {
    if(req.headers.authorization) {
        const token = req.headers.authorization.split(' ');
        const user = decodeToken(token[1]);
        if(user.isBusiness === 'true' && user.roleId === 'company') {
            req.businessId = user._id;
            return next()
        }
        return res.status(401).json({ message: fail.NOT_AUTH });
    }
    return res.status(401).json({ message: fail.NOT_TOKEN }); 
};

const authenticateBusiness = (req, res, next) => {
    const { businessId } = req;
    const { id } = req.params;
    if(id === businessId){
        return next(); 
    }
    return res.status(401).json({ message: fail.NOT_AUTH })
}

module.exports = {
    validateBusiness,
    authenticateBusiness
};
