const jwt = require('jsonwebtoken');
const decodeToken = require('../services/internals/decodeToken');
const { fail } = require('../services/internals/constants');

const validateUser = (req, res, next) => {
    if(req.headers.authorization) {
        const token = req.headers.authorization.split(' ');
        const user = decodeToken(token[1]);
        if(user.isBusiness === 'false' && user.roleId === 'client') {
            req.userId = user._id;
            return next()
        }
        return res.status(401).json({ message: fail.NOT_AUTH });
    }
    return res.status(401).json({ message: fail.NOT_TOKEN });
};

const authenticateUser = (req, res, next) => {
    const { userId } = req;
    const { id } = req.params;
    if(id === userId){
        return next(); 
    }
    return res.status(401).json({ message: fail.NOT_AUTH })
}

module.exports = {
    validateUser,
    authenticateUser
};
