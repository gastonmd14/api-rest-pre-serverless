const mongoose = require('mongoose');
const { Decimal128 } = require('bson');

const invoiceSchema = new mongoose.Schema({
    businessId: {
        type: String, 
        required: true
    },
    userId: {
        type: String,
        required: false,
        default: ''
    },
    product: {
        type: String, 
        required: true
    },
    price: {
        type: Decimal128, 
        required: true 
    },
    status: {
        type: String,
        enum: ['pending', 'completed', 'blocked'],
        default: 'pending'
    },
    record: {
        type: Number,
        default: 0
    }
});

const InvoiceModel = mongoose.model(
    'Invoice',
    invoiceSchema,
    'invoices'
);

module.exports = InvoiceModel;
