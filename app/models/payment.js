const mongoose = require('mongoose');

const paymentSchema = new mongoose.Schema({
    userId: {
        type: String, 
        required: true
    },
    invoiceId: {
        type: String, 
        required: true
    }
});

const PaymentModel = mongoose.model(
    'Payment',
    paymentSchema,
    'payments'
);

module.exports = PaymentModel;
