const mongoose = require('mongoose');
const { Decimal128 } = require('bson');

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    isBusiness: {
        type: String,
        required: true
    },
    roleId: {
        type: String,
        default: 'client'
    },
    card: {
        limit: {
            type: Decimal128,
            default: 50000.00
        },
        status: {
            type: String,
            eanum: ['active', 'desactive'],
            default: 'active'
        },
        debt: {
            type: Decimal128,
            required: false,
            default: 0.00
        }
    }
});

const UserModel = mongoose.model(
    'User',
    userSchema,
    'users'
);

module.exports = UserModel;
