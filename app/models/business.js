const mongoose = require('mongoose');

const businessSchema = new mongoose.Schema({
    email: {
        type: String, 
        required: true
    },
    password: {
        type: String, 
        required: true 
    },
    isBusiness: {
        type: String, 
        required: true 
    },
    roleId: {
        type: String, 
        default: 'company' 
    }
});

const BusinessModel = mongoose.model(
    'Business',
    businessSchema,
    'businesses'
);

module.exports = BusinessModel;
